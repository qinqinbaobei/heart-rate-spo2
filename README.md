# HeartRateSPO2

#### 介绍
生命体征监测仪，温度，心率，血氧饱和度，血氧波形，使用MAX30102模块，基于Arduino IDE
![输入图片说明](image/MAX30102.png)

#### 软件架构
下位机硬件部分：树莓派Pico 使用Arduino IDE编程

上位机软件部分：Python 使用DearpyGui框架绘制桌面客户端


#### 运行教程
1. 单片机烧录程序（HeartRate_SPO2_arduino/HeartRate_SPO2_arduino.ino/HeartRate_SPO2_arduino.ino）
2. PC安装Python依赖库，在（HeartRate_SPO2/HeartRate_SPO2_PC）目录

   `pip install -r requirements.txt`

3. 运行上位机程序代码（HeartRate_SPO2/HeartRate_SPO2_PC/main.py），会自动识别可用串口，如串口设备较多识别的串口不正确，在窗口下方切换
4. PC程序有socket服务，有连接时不间断发送数据，端口：9999，连接示例：（HeartRate_SPO2/HeartRate_SPO2_PC/client.py）

#### 使用说明
1.  MAX30102连接电源，通过IIC连接单片机

- Raspberrypi Pico 电路连接 IO4->SDA，IO5->SCL
![输入图片说明](image/Raspberrypi_Pico.png)

- Arduino Nano、Uno 电路连接 A4->SDA，A5->SCL
![输入图片说明](image/Arduino_Nano.png)

#### 注意事项
1. MAX30102在手指按压时易触碰到模块上的外围电路引起干扰和短路，使用时需要将外围电路隔离，例如：贴胶带。
2. MAX30102是反射式传感器，精度相较于透射式传感器略低，也会因手指按压力度影响测量结果，结果仅供参考，不能作为临床诊断依据。

#### 演示
![输入图片说明](image/%E6%BC%94%E7%A4%BA.gif)
![输入图片说明](image/%E6%88%AA%E5%9B%BE1.png)
![输入图片说明](image/%E6%88%AA%E5%9B%BE2.png)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
