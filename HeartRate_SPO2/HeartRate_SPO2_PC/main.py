import threading
import dearpygui.dearpygui as dpg
import numpy as np
import Max_30102
import serial
import winsound
import time
import json

# 采样数
Samplerate = 200


max_device = Max_30102.Device(sample_rate=Samplerate)
dpg.create_context()


with dpg.font_registry():
    default_font = dpg.add_font("simhei.ttf", 18)
    big_font = dpg.add_font("simhei.ttf", 100)


with dpg.theme() as global_theme:
    with dpg.theme_component(dpg.mvAll):
        dpg.add_theme_color(
            dpg.mvThemeCol_FrameBg, (0, 0, 0), category=dpg.mvThemeCat_Core
        )
        dpg.add_theme_color(
            dpg.mvThemeCol_WindowBg, (0, 0, 0), category=dpg.mvThemeCat_Core
        )
        dpg.add_theme_color(
            dpg.mvPlotCol_Line, (255, 255, 0), category=dpg.mvThemeCat_Plots
        )
        dpg.add_theme_color(
            dpg.mvPlotCol_FrameBg, (255, 255, 255), category=dpg.mvThemeCat_Core
        )
        dpg.add_theme_style(
            dpg.mvStyleVar_FrameRounding, 5, category=dpg.mvThemeCat_Core
        )


def update_data():
    while True:
        max_device.get_data()
        dpg.set_value("rawIR", max_device.ir_list_data)
        if max_device.data_valid:
            data_np = np.array(max_device.ir_list_data_filtered)
            data_np = data_np + abs(min(data_np))
            IR_line_p = 137 + 233 * (data_np[-4] / (max(data_np) - min(data_np)))
            dpg.configure_item("IR_line", p1=(830, IR_line_p))
            dpg.set_value(
                "series_tag",
                [
                    max_device.range_list_time,
                    max_device.ir_list_data_filtered,
                ],
            )
        dpg.set_value(
            "Temperature_text",
            "Temperature: {:.1f} °C".format(max_device.data["temperatureC"]),
        )
        dpg.set_value("bpm_text", "BPM: {:.0f} ".format(max_device.data["HR"]))
        dpg.set_value("SPO2_text", "SPO2: {:.1f} %".format(max_device.data["SPO2"]))
        # dpg.set_value("IR_text", "IR: {}".format(max_device.data["ir"]))
        # dpg.set_value("RED_text", "RED: {}".format(max_device.data["red"]))


def port_choose():
    max_device.valid = False
    max_device.ser.close()
    max_device.port = dpg.get_value("port_list")
    max_device.ser = serial.Serial(max_device.port, max_device.brate)
    max_device.valid = True


with dpg.window(tag="primary", label="IR Graph") as win_prima:
    dpg.add_simple_plot(
        label="", tag="rawIR", indent=25, default_value=(0, 0), height=100, width=800
    )
    IR_line = dpg.draw_line(
        (830, 137), (830, 370), tag="IR_line", color=(255, 255, 0, 255), thickness=12
    )
    with dpg.plot(label="SPO2-IR", height=300, width=820):
        # optionally create legend
        dpg.add_plot_legend()

        # REQUIRED: create x and y axes
        dpg.add_plot_axis(dpg.mvXAxis, tag="x_axis")
        dpg.add_plot_axis(dpg.mvYAxis, tag="y_axis")
        dpg.set_axis_limits("x_axis", 0, Samplerate)

        # series belong to a y axis
        dpg.add_line_series(
            max_device.data_list_time,
            max_device.ir_list_data_filtered,
            parent="y_axis",
            tag="series_tag",
        )
    with dpg.drawlist(width=850, height=100):
        dpg.draw_rectangle(
            (0, 0),
            (850, 100),
            tag="beep_view",
            color=(255, 0, 0, 255),
            fill=(255, 0, 0, 255),
            show=False,
        )
    BPM_text = dpg.add_text("BPM:", pos=(0, 413), indent=20, tag="bpm_text")
    SPO2_text = dpg.add_text("SPO2:", indent=20, tag="SPO2_text")
    dpg.add_text("Temperature:", indent=20, tag="Temperature_text")
    port_list_combo = dpg.add_combo(
        items=max_device.port_list,
        width=200,
        indent=20,
        callback=port_choose,
        tag="port_list",
    )
    dpg.set_value("port_list", max_device.port)
    # dpg.add_text("IR:", tag="IR_text")
    # dpg.add_text("RED:", tag="RED_text")
    dpg.bind_font(default_font)
    dpg.bind_item_font(BPM_text, big_font)
    dpg.bind_item_font(SPO2_text, big_font)


def HR_beep():
    while True:
        while max_device.k < 0:
            time.sleep(0.01)
        while max_device.k >= 0:
            time.sleep(0.01)
        winsound.Beep(2000, 150)
        dpg.configure_item("beep_view", show=True)
        time.sleep(0.1)
        dpg.configure_item("beep_view", show=False)


def server_run():
    import socket

    ip_port = ("0.0.0.0", 9999)

    sk = socket.socket()  # 创建套接字
    sk.bind(ip_port)  # 绑定服务地址
    sk.listen(1)  # 监听连接请求
    print("等待客户端连接")
    while True:
        conn, address = sk.accept()  # 等待连接，此处自动阻塞
        print("客户端已连接", address)
        while True:
            try:
                conn.sendall(json.dumps(max_device.data).encode())
                time.sleep(0.05)
            except:
                conn.close()  # 关闭连接
                print("客户端已断开")
                break


threading.Thread(target=update_data, daemon=True).start()
threading.Thread(target=HR_beep, daemon=True).start()
threading.Thread(target=server_run, daemon=True).start()
dpg.bind_theme(global_theme)
dpg.create_viewport(title="Heart Rate And SPO2", width=885, height=800)
dpg.set_primary_window("primary", True)
dpg.setup_dearpygui()
dpg.show_viewport()
dpg.start_dearpygui()
dpg.destroy_context()
