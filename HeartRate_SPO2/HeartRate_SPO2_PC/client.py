import socket
import json
import matplotlib.pyplot as plt
import threading

# 采样数
Samplerate = 100

plt.ion()
xs = []
ys = []

ip_port = ("127.0.0.1", 9999)
s = socket.socket()  # 创建套接字
s.connect(ip_port)  # 连接服务器

def update_data():
    global ys
    while True:
        server_reply = json.loads(s.recv(1024).decode())
        print(server_reply)
        if len(xs) > Samplerate:
            xs.pop(0)
        if len(ys) > Samplerate:
            ys.pop(0)
        xs.append(server_reply["Time"])
        ys.append(server_reply["ir"])

threading.Thread(target=update_data, daemon=True).start()


while True:  # 通过一个死循环不断接收用户输入，并发送给服务器
    if len(xs) > Samplerate:
        plt.clf()
        plt.plot(xs, ys ,"red")
        plt.pause(0.05)

s.close()  # 关闭连接
